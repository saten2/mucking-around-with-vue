window.addEventListener('DOMContentLoaded', init);

const SOCIAL_SECRUITY_TAX_RATE = 0.062;
const SOCIAL_SECRUITY_TAX_CAP = 137700;
const MEDICARE_TAX_RATE = 0.0145;
const MEDICARE_ADDITIONAL_TAX_RATE = 0.009;
const MEDICARE_ADDITIONAL_TAX_INCOME = 200000;

const STANDARD_DEDUCTION = 12400;

const TAX_BRACKETS = [
  { cap: 9875,      rate: 0.10, },
  { cap: 40125,     rate: 0.12, },
  { cap: 85525,     rate: 0.22, },
  { cap: 163300,    rate: 0.24, },
  { cap: 207350,    rate: 0.32, },
  { cap: 518400,    rate: 0.35, },
  { cap: Infinity,  rate: 0.37, },
];

function calcFicaTax(salary) {
  const ssTaxable = Math.min(salary, SOCIAL_SECRUITY_TAX_CAP);
  const ssOwed = ssTaxable * SOCIAL_SECRUITY_TAX_RATE;
  const mcOwed = salary * MEDICARE_TAX_RATE;
  const mcAddTaxable = Math.max(0, salary - MEDICARE_ADDITIONAL_TAX_INCOME);
  const mcAddOwed = mcAddTaxable * MEDICARE_ADDITIONAL_TAX_RATE;
  return {
    socialSecurity: {
      taxable: ssTaxable,
      owed: ssOwed,
    },
    medicare: {
      baseOwed: mcOwed,
      additionalTaxable: mcAddTaxable,
      additionalOwed: mcAddOwed,
      owed: mcOwed + mcAddOwed,
    },
    owed: ssOwed + mcOwed + mcAddOwed,
  }
}

function calcIncomeTax(salary) {
  const taxableSalary = salary - STANDARD_DEDUCTION;
  let totalOwed = 0;

  var previousCap = 0;
  TAX_BRACKETS.forEach(function(bracket) {
    const cap = Math.min(taxableSalary, bracket.cap);
    const taxableAtThisBracket = cap - previousCap;
    const owedAtThisBracket = taxableAtThisBracket * bracket.rate;
    totalOwed += owedAtThisBracket;
    previousCap = cap;
  })

  return {
    taxable: taxableSalary,
    owed: totalOwed,
  }
}

function calcPostTax(salary) {
  return salary - calcFicaTax(salary).owed - calcIncomeTax(salary).owed;
}

// sample salary of 70K
// fica: 70000 * (0.062 + 0.0145) = 5,355
// taxable: 70000 - 12400 = 57600
// federal: 9875 * 0.10 + 30,250 * 0.12 + 17475 * 0.22 = 8462
function testCalcs() {
  const testSalary = 70000;
  const expectedFica = 5355;
  const incomeExpected = 8462;

  const ficaResult = calcFicaTax(testSalary);
  const incomeResult = calcIncomeTax(testSalary);
  console.log(
    `fica expected: ${expectedFica}, actual: ${ficaResult.owed}`
  );
  console.log(
    `income expected: ${incomeExpected}, actual: ${incomeResult.owed}`
  );
}

function init() {
  var savedSalariesString = window.localStorage.getItem('salaries', '[]');
  var savedSalaries = JSON.parse(savedSalariesString);

  var vm = new Vue({
    el: '#app',
    data: {
      title: 'Hello and welcome to my kitchen!',
      newSalary: 10000,
      salaries: savedSalaries,
      error: '',
    },
    methods: {
      addSalary: function() {
        if (this.salaries.indexOf(this.newSalary) != -1) {
          this.error = "You already entered that value.";
          return;
        }
        this.salaries.push(this.newSalary);
        this.newSalary = 10000;
        this.error = '';
      },
      removeSalary: function(salary) {
        var index = this.salaries.indexOf(salary);
        this.salaries.splice(index, 1);
      }
    },
    watch: {
      salaries: function(newSalaries) {
        window.localStorage.setItem(
          'salaries',
          JSON.stringify(this.salaries)
        );
      }
    }
  })
}

Vue.component('salary', {
  props: ['salary'],
  template: `
    <div class="salary">
      <h2>{{ salary }} ({{ actual }})</h2>
      <p v-for="percent in [0.50, 0.30, 0.20]">
        <em>{{ percent*100 }}:</em> {{ getSalaryPercent(percent).toLocaleString() }}
      </p>
      <button v-on:click="$emit('delete', salary)">delete</button>
    </div>
  `,
  computed: {
    actual: function() {
      return calcPostTax(this.salary)
    },
    monthly: function() {
      return this.actual / 12;
    },
  },
  methods: {
    getSalaryPercent: function(percent) {
      return Math.floor(this.monthly * percent);
    }
  }
});
